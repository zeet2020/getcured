
require 'rubygems'
require 'sinatra/base'
require 'sinatra/flash'
require 'mongoid'
require './controllers/application_controller.rb'


Dir.glob('./{models,controllers}/*.rb').each { |file| require file }

map('/admin'){ run ManageController }
map('/patient'){ run PatientController }
map('/') { run AppController }
