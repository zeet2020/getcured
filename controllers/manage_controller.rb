class ManageController < AppController


get '/patient' do

     @patient = Patient.all
     @patient.to_json

end

get '/patient/:id' do |id|

  @patient = Patient.where(id: id)
  @patient.to_json

end

post '/patient' do


end

put '/patient/:id' do |id|
  @patient = Patient.where(id:"#{id}")
  data = JSON.parse(request.body.read.to_s)
  data.delete('_id')
  if @patient
    @patient.to_json if @patient.update(data)
    @patient.to_json
  end
end


get '/dashboard' do
  require_logged_in
  erb :dash

end





post '/dashboard' do

     upload_path = options.upload_path
     filename = params['upload_file'][:filename]
     File.open(upload_path+'/'+filename,'w') do |f|
         f.write(params['upload_file'][:tempfile].read)
     end
     #File.open(upload_path+'/'+filename,'w')
     fp = File.open(upload_path+'/'+filename,'r')
     documents = JSON.parse fp.read.to_s
     patient_attr = ['city','age','doctor','email','first_name','last_name','mobile']
     disease_attr = ['symptom', 'prescription','detected_disease','cured']
     documents = [documents] if documents.class == Hash
     count = 0
     error_mesg = ''
     success_mesg = ''
     documents.each do |doc|
       count = count + 1
       skip = 0
       patient_attr.each do |item|

         if not doc.has_key? item

           error_mesg << "require attribute is missing in record #{count}</br>"
           skip = 1
           break
         end
        end

        next if skip == 1
        @p = Patient.new(doc)
        success_mesg << "record #{count} is imported success " if @p.save
     end
     flash[:danger] = error_mesg if error_mesg.length > 0
     flash[:success] = success_mesg if success_mesg.length > 0
     redirect "/admin/dashboard"
end





end
