class PatientController < AppController




get '/' do
   require_logged_in
   if params['searchterm']
     searchterm = params['searchterm']
     @patients = Patient.where('$or' => [{mobile: "#{searchterm}"},{first_name: "#{searchterm}"},{last_name: "#{searchterm}"}])
   else
    @patients = Patient.all
   end

   erb :list
end






get '/new' do

    erb :new
end


post '/new' do


  patient_attr = ['city','age','doctor','email','first_name','last_name','mobile']
  error_mesg = ''
  patient_attr.each do |item|

       error_mesg << "#{item} is required field </br>" if not (params.has_key?(item) and params[item].length > 0)

  end

  if error_mesg.length > 0

     flash[:danger] = error_mesg
     redirect redirect '/patient/new'
  end


  @patient  = Patient.new params
  @patient.diseases = []
  flash[:success] = "patient was created " if @patient.save
  if is_authenticated?
     redirect '/patient'
   else
     redirect '/login'
  end
end


get '/:id' do |id|
  require_logged_in
  @id = id;

  erb :update

end


get '/:id/delete' do |id|
  require_logged_in
  @patient = Patient.where(id:"#{id}")
  @patient.destroy
  redirect "/patient"

end





end
