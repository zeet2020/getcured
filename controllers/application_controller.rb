require 'rubygems'
require 'erb'
require 'json'





class AppController < Sinatra::Base

set :views, File.expand_path('./views')
set :public_folder, File.expand_path('./public')
enable :sessions
set :session_secret, 'awesome'
register Sinatra::Flash
set :admin_username, "admin"
set :admin_password, "password"
set :environment, :development
set :upload_path, File.expand_path('./public/upload')

Mongoid.load!(File.expand_path("./mongoid.yml"))

def is_authenticated?
  return !!session[:current_user]
end

def require_logged_in

    redirect '/login' unless is_authenticated?

end

def doctor_list # may be dynamic
    list = {}
    list["john steve"] = "john steve"
    list["gopal prasad"] = "gopal prasad"
    list["srinivasan"]  = "srinivasan"
    list
end




not_found do

 'Not Found!'

end


get '/' do

erb :index

end


get '/login' do
  redirect '/patient' if is_authenticated?
  erb :login
end

post '/login' do
  username = options.admin_username
  password = options.admin_password
  if params['username'] == username and params['password'] == password
    session[:current_user] = username

    redirect '/patient'
  else
    flash[:danger] = "invalid username or password"
    redirect '/login'
  end
end

get '/logout' do

  session.clear
  redirect '/'
end


end
