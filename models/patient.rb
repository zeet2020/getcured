class Patient
    include Mongoid::Document

    field :first_name, type: String
    field :last_name, type: String
    field :doctor, type: String
    field :email,  type: String
    field :address_1, type: String
    field :address_2, type: String
    field :city, type: String
    field :date, type: DateTime
    field :age, type: Integer
    field :mobile, type: Integer
    field :diseases, type: Array

end
