#POC implementation of Patient management system


#Table of Content

1. implemention stacks

2. features

3. UI

4. TODO

5. how to run the application




### 1. Implementation Stacks

Implementation stack consist of ruby, mongodb, sinatra DSL, mongoid (ODM for ruby)

sinatra DSL: is ruby base dsl, used to implement the UI for application which handle the admin login and creating patient records

mongoid : is ODM layer for ruby used to store and retereive documents on mongodb, mongoid is used to store patient records as document and provide
flexible model which could provide a feasibility for creating of dynamic fields.

system design and stack was considered based on requirement and specific need of application.



### 2. features

1. allow admin to create patient records, update records and delete records
2. allow admin to add/remove  dynamic section and  fields
3. allow admin to create patient record by uploading .json file
4. allow admin to search patient info my first_name, last_name and mobile number

### 3. UI

#####Manage Patient : allow admin to search and list the patient
#####Register Patient : allow admin to create a new patient record by inputing on a web form
#####upload records : allow admin to create patient record by uploading a json file
#####patient update page: allow admin to see the patient information and medical history and update history

### 4. TODO

1. muliple level and advance level of error handling
2. support multiple different upload file formate lie (xsl, csv)
3. adding appointment management system
4. login for doctors and patient
5. more support for dynamic fields

### 5. how to run application

running the the application is simple (make sure you have ruby install on your system)

#####step 1. clone the repo (https://bitbucket.org/zeet2020-admin/getcured)
#####step 2. run bundler 'bundle install' (bundle will install require gem)
#####step 3. once the bundle is completed just run 'rackup' (application will run at port "9292")

#####note: please sure that your running the mongo deamon before you start of application

login infomation for admin

>>username: "admin"
>>password: "password"

###hosted on openshift  https://getcured-codingcatt.rhcloud.com
